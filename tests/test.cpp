#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "consumer.h"
#include "mqp.h"

#include <chrono>
#include <thread>

namespace {

    template <class Key, class Value>
    class TestConsumer : public NMqp::IConsumer<Key, Value> {
        public:
            void Consume(Key id, Value&& value) noexcept override {
                values_.emplace_back(std::move(value));
                ++consumed_count_;
            }

            const std::vector<Value>& Values() const {
                return values_;
            }

            size_t ConsumedCount() const {
                return consumed_count_;
            }

        private:
            std::vector<Value> values_;
            std::atomic<size_t> consumed_count_;
    };

} // anonymous namespace

TEST(TestStartStop, Simple) {
    NMqp::MQP<int, int> mqp(1000, 1);
}

TEST(TestEnqueue, Simple) {
    NMqp::MQP<int, int> mqp(1000, 1);
    mqp.Enqueue(1, 2);
}

TEST(TestConsumer, EnqueueFirst) {
    auto consumer = std::make_shared<TestConsumer<int, int>>();
    {
        NMqp::MQP<int, int> mqp(1000, 1);
        mqp.Enqueue(1, 2);
        mqp.Enqueue(1, 3);
        mqp.Subscribe(1, consumer);
    }
    EXPECT_THAT(consumer->Values(), ::testing::ElementsAre(2, 3));
}

TEST(TestConsumer, ConsumeFirst) {
    auto consumer = std::make_shared<TestConsumer<int, int>>();
    {
        NMqp::MQP<int, int> mqp(1000, 1);
        mqp.Subscribe(1, consumer);
        mqp.Enqueue(1, 4);
        mqp.Enqueue(1, 5);
    }
    EXPECT_THAT(consumer->Values(), ::testing::ElementsAre(4, 5));
}

TEST(TestConsumer, Unsubscribe) {
    auto consumer = std::make_shared<TestConsumer<int, int>>();
    {
        NMqp::MQP<int, int> mqp(1000, 1);
        mqp.Subscribe(1, consumer);
        mqp.Enqueue(1, 4);
        mqp.Enqueue(1, 5);
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
        mqp.Unsubscribe(1);
        mqp.Enqueue(1, 6);
        mqp.Enqueue(1, 7);
    }
    EXPECT_THAT(consumer->Values(), ::testing::ElementsAre(4, 5));
}

TEST(TestConsumer, WaitBeforeEnqueue) {
    NMqp::MQP<int, int> mqp(1000, 1);
    auto consumer = std::make_shared<TestConsumer<int, int>>();
    mqp.Subscribe(1, consumer);
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    mqp.Enqueue(1, 4);
    mqp.Enqueue(1, 5);
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    EXPECT_EQ(consumer->ConsumedCount(), 2);
}

TEST(TestConsumer, MultipleProducers) {
    auto consumer = std::make_shared<TestConsumer<int, int>>();
    {
        NMqp::MQP<int, int> mqp(10000, 10);
        mqp.Subscribe(1, consumer);
        std::vector<std::future<void>> futures;
        for (int i = 0; i < 100; ++i) {
            futures.push_back(std::async(std::launch::async, [&mqp]() {
                for (int j = 0; j < 100; ++j) {
                    mqp.Enqueue(1, j);
                }
            }));
        }
        for (auto& future : futures) {
            future.wait();
        }
    }
    EXPECT_EQ(consumer->ConsumedCount(), 10000);
}

TEST(TestParallel, ManyQueues) {
    static const int key_count = 10000;
    static const int element_count = 100;

    std::vector<std::shared_ptr<TestConsumer<int, int>>> consumers;
    for (int i = 0; i < key_count; ++i) {
        consumers.push_back(std::make_shared<TestConsumer<int, int>>());
    }

    {
        NMqp::MQP<int, int> mqp(1000, 4);
        for (int i = 0; i < key_count; ++i) {
            mqp.Subscribe(i, consumers[i]);
        }

        for (int j = 0; j < element_count; ++j) {
            for (int i = 0; i < key_count; ++i) {
                mqp.Enqueue(i, j);
            }
        }
    }

    for (const auto& consumer : consumers) {
        EXPECT_EQ(consumer->ConsumedCount(), element_count);
    }
}

TEST(TestParallel, ManyQueuesMultipleProducers) {
    static const int key_count = 100;

    std::vector<std::shared_ptr<TestConsumer<int, int>>> consumers;
    for (int i = 0; i < key_count; ++i) {
        consumers.push_back(std::make_shared<TestConsumer<int, int>>());
    }

    {
        NMqp::MQP<int, int> mqp(10000, 4);
        for (int i = 0; i < key_count; ++i) {
            mqp.Subscribe(i, consumers[i]);
        }

        std::atomic<int> count = 0;
        std::vector<std::future<void>> futures;
        for (int i = 0; i < 10; ++i) {
            futures.push_back(std::async(std::launch::async, [&mqp, &count]() {
                for (int j = 0; j < 1000; ++j) {
                    const auto id = (++count) % key_count;
                    mqp.Enqueue(id, 1);
                }
            }));
        }

        for (auto& future : futures) {
            future.wait();
        }
    }

    int consumed = 0;
    for (const auto& consumer : consumers) {
        consumed += consumer->ConsumedCount();
    }
    EXPECT_EQ(consumed, 10000);
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
