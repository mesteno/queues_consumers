# Queues consumers test project

Some notes:
1. It is guaranteed that all values will pass to consumers in enqueue order;
1. All values will pass to consumer consequently, so it is not necessary for consumer to be thread-safe;
1. Before destroyinq MQP all subscribed queues will be processed;

To run tests:
```
mkdir build
cd build
cmake ..
make
./tests/mqp_test
```
