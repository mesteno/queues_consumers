#pragma once

#include "consumer.h"
#include "shard_processor.h"

#include <boost/asio/thread_pool.hpp>

#include <iostream>
#include <queue>
#include <shared_mutex>
#include <unordered_map>

namespace NMqp {

    template <class Key, class Value>
    class MQP {
        using ShardProcessorPtr = std::shared_ptr<ShardProcessor<Key, Value>>;

        public:
            MQP(size_t queue_capacity, size_t thread_count)
                : queue_capacity_(queue_capacity)
                , thread_pool_(thread_count)
                , thread_(&MQP::Process, this)
            { }

            MQP(const MQP&) = delete;
            MQP& operator=(const MQP&) = delete;

            ~MQP() {
                Stop();
                thread_.join();
                thread_pool_.join();
            }

            void Subscribe(Key id, IConsumerPtr<Key, Value> consumer) {
                GetOrAddShardProcessor(id)->RegisterConsumer(std::move(consumer));
            }

            void Unsubscribe(Key id) {
                const auto shard_processor = GetShardProcessor(id);
                if (shard_processor) {
                    shard_processor->UnregisterConsumer();
                }
            }

            template <typename ValueType>
            void Enqueue(Key id, ValueType&& value) {
                GetOrAddShardProcessor(id)->Push(std::forward<Value>(value));
                signal_cv_.notify_one();
            }

        private:
            void Stop() {
                auto lock = std::unique_lock{mutex_};
                stopped_ = true;
                signal_cv_.notify_one();
            }

            ShardProcessorPtr GetShardProcessor(Key id) {
                const auto lock = std::shared_lock{mutex_};
                auto it = shard_processors_.find(id);
                return it == shard_processors_.end() ? nullptr : it->second;
            }

            ShardProcessorPtr GetOrAddShardProcessor(Key id) {
                const auto shard_processor = GetShardProcessor(id);
                if (shard_processor) {
                    return shard_processor;
                }

                const auto lock = std::unique_lock{mutex_};
                auto it = shard_processors_.find(id);
                if (it == shard_processors_.end()) {
                    shard_processors_.emplace(id, std::make_shared<ShardProcessor<Key, Value>>(id, queue_capacity_));
                    it = shard_processors_.find(id);
                }
                return it->second;
            }

            bool ReadyToConsumeUnsafe() const {
                for (auto& [_, shard_processor] : shard_processors_) {
                    if (shard_processor->ReadyToConsume()) {
                        return true;
                    }
                }
                return false;
            }

            void Process() {
                while (!stopped_) {
                    auto lock = std::shared_lock{mutex_};
                    signal_cv_.wait(lock, [this] { return stopped_ || ReadyToConsumeUnsafe(); });

                    for (auto& [_, shard_processor] : shard_processors_) {
                        shard_processor->Process(thread_pool_);
                    }
                }

                const auto lock = std::shared_lock{mutex_};
                for (auto& [_, shard_processor] : shard_processors_) {
                    shard_processor->Process(thread_pool_, true);
                }
            }

        private:
            size_t queue_capacity_;
            boost::asio::thread_pool thread_pool_;
            std::thread thread_;

            std::unordered_map<Key, ShardProcessorPtr> shard_processors_;
            std::atomic<bool> stopped_{};
            std::condition_variable_any signal_cv_;
            std::shared_mutex mutex_;
    };

} // namespace NMqp
