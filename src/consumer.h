#pragma once

#include <memory>

namespace NMqp {

    template <class Key, class Value>
    class IConsumer {
        public:
            virtual ~IConsumer() = default;

            virtual void Consume(Key key, Value&& value) noexcept = 0;
    };

    template <class Key, class Value>
    using IConsumerPtr = std::shared_ptr<IConsumer<Key, Value>>;

} // namespace NMqp
