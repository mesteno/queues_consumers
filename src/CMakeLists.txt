set(target mqp)

set(srcs
    consumer.cpp
    mqp.cpp
    shard_processor.cpp)

set(headers
    consumer.h
    mqp.h
    shard_processor.h)

add_library(${target}
    ${srcs}
    ${headers})

target_link_libraries(${target} PUBLIC
    ${Boost_LIBRARIES})
