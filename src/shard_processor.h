#pragma once

#include "consumer.h"

#include <boost/asio.hpp>
#include <boost/asio/thread_pool.hpp>

#include <iostream>
#include <mutex>
#include <queue>

namespace NMqp {

    template <class Key, class Value>
    class ShardProcessor {
        public:
            ShardProcessor(Key id, size_t capacity)
                : id_(id)
                , capacity_(capacity)
            { }

            void RegisterConsumer(IConsumerPtr<Key, Value> consumer) {
                const auto lock = std::unique_lock{mutex_};
                consumer_ = consumer;
            }

            void UnregisterConsumer() {
                const auto lock = std::unique_lock{mutex_};
                consumer_.reset();
            }

            bool Push(Value&& value) {
                const auto lock = std::unique_lock{mutex_};
                if (values_.size() < capacity_) {
                    values_.push(std::forward<Value>(value));
                    return true;
                }
                return false;
            }

            bool ReadyToConsume() const {
                const auto lock = std::unique_lock{mutex_};
                return consumer_ && !values_.empty();
            }

            void Process(boost::asio::thread_pool& thread_pool, bool force = false) {
                if (!ReadyToConsume()) {
                    return;
                }

                if (consuming_) {
                    if (force) {
                        ready_.wait();
                    } else {
                        return;
                    }
                }

                consuming_ = true;
                std::promise<void> promise;
                ready_ = promise.get_future();

                const auto lock = std::unique_lock{mutex_};
                const auto size = values_.size();

                boost::asio::post(
                        thread_pool,
                        [this, consumer = consumer_, promise = std::move(promise), values = std::move(values_)]() mutable {
                    while (!values.empty()) {
                        consumer->Consume(id_, std::move(values.front()));
                        values.pop();
                    }
                    consuming_ = false;
                    promise.set_value();
                });

                values_ = std::queue<Value>{};
            }

        private:
            Key id_;
            size_t capacity_;
            IConsumerPtr<Key, Value> consumer_;
            std::queue<Value> values_;
            std::future<void> ready_;
            std::atomic<bool> consuming_{};
            mutable std::mutex mutex_;
    };

} // namespace NMqp
